#include <errno.h>
#include <stdlib.h>
#include <string.h>

#include <epicsExport.h>
#include <epicsThread.h>
#include <errlog.h>
#include <iocsh.h>

#include "drvAsynFile.h"

static const char *driverName = "drvAsynFile";

#define ASYN_ERROR(fmt, args...)  epicsSnprintf(pasynUser->errorMessage, pasynUser->errorMessageSize, fmt, ##args)
#define STX 0x02
#define ETX 0x03


drvAsynFile::drvAsynFile(const char *portname, const char *path_to_expose, int max_filesize, bool no_iointr_pattern_match) :
        asynPortDriver(portname,
                       3,                                  /* maxAddr */
                       0,                                  /* paramTableSize */
                       asynOctetMask,                      /* interfaceMask */
                       asynOctetMask,                      /* interruptMask */
                       ASYN_CANBLOCK | ASYN_MULTIDEVICE,   /* asynFlags */
                       1,                                  /* autoConnect */
                       0,                                  /* priority */
                       0),                                 /* stackSize */
        m_ExposedPath(path_to_expose),
        m_Pattern(),
        m_Filename(),
        m_PatternMatchPointer(),
        m_FilenameReadPointer(),
        m_FilenameRead(FLUSH),
        m_PatternMatchRead(FLUSH),
        m_InSession(false),
        m_Glob(NULL),
        m_IOIntrPatternMatch(!no_iointr_pattern_match),
        m_MaxFilesize(0),
        m_NumBytesRead(0)
{
    m_ExposedPath.append("/");
    setMaxFilesize(max_filesize);
}


asynStatus drvAsynFile::lock()
{
    return asynPortDriver::lock();
}


asynStatus drvAsynFile::unlock()
{
    return asynPortDriver::unlock();
}


asynStatus drvAsynFile::flushOctet(asynUser *pasynUser)
{
    return asynSuccess;
}


asynStatus drvAsynFile::readOctet(asynUser *pasynUser, char *value, size_t maxChars, size_t *nActual, int *eomReason)
{
    int addr;
    asynStatus status;

    *nActual = 0;

    status = getAddress(pasynUser, &addr);
    if (status != asynSuccess)
        return status;

    int leomReason;

    if (eomReason == NULL)
        eomReason = &leomReason;

    switch (addr)
    {
            case List:
                    if (m_IOIntrPatternMatch)
                    {
                        *nActual = 0;
                        *eomReason = ASYN_EOM_END;

                        return asynTimeout;
                    }

                    return readPatternMatch(pasynUser, value, maxChars, nActual, eomReason);

            case Filename:
                    return readFilename(pasynUser, value, maxChars, nActual, eomReason);

            case Data:
                    return readFile(pasynUser, value, maxChars, nActual, eomReason);
    }

    ASYN_ERROR("Unknown address: %d!", addr);

    return asynError;
}


asynStatus drvAsynFile::writeOctet(asynUser *pasynUser, const char *value, size_t maxChars, size_t *nActual)
{
    int addr;
    asynStatus status;

    status = getAddress(pasynUser, &addr);
    if (status != asynSuccess)
        return status;

    switch (addr)
    {
            case List:
                    return writePattern(pasynUser, value, maxChars, nActual);

            case Filename:
                    return writeFilename(pasynUser, value, maxChars, nActual);

            case Data:
                    return asynDisabled;
    }

    ASYN_ERROR("Unknown address: %d!", addr);

    return asynError;
}


void drvAsynFile::setMaxFilesize(size_t max_filesize)
{
    if (max_filesize >= 0)
        m_MaxFilesize = max_filesize;
}


asynStatus drvAsynFile::readPatternMatch(asynUser *pasynUser, char *value, size_t maxChars, size_t *nActual, int *eomReason)
{
    int advancePointer;

    switch (m_PatternMatchRead)
    {
        case FLUSH:
            *nActual = 0;
            *eomReason = ASYN_EOM_END;
            return asynSuccess;

        case NORMAL:
            createGlob();

            if (m_PatternMatchPointer == NULL || m_PatternMatchPointer[0] == '\0')
                m_PatternMatchPointer = m_Glob->getNextFilename();

            if (m_PatternMatchPointer[0] != '\0')
            {
                advancePointer = strlen(m_PatternMatchPointer);
                *nActual = advancePointer + 1;

                if (*nActual > maxChars)
                {
                    advancePointer = *nActual = maxChars;
                    *eomReason = ASYN_EOM_CNT;
                }
                else
                {
                    *eomReason = ASYN_EOM_END;
                }

                strncpy(value, m_PatternMatchPointer, *nActual);
                m_PatternMatchPointer += advancePointer;

                return asynSuccess;
            }

            delete m_Glob;
            m_Glob = NULL;

            m_PatternMatchRead = EOS;
            *eomReason = ASYN_EOM_END;

            return asynSuccess;

        case EOS:
            *nActual   = 0;
            *eomReason = ASYN_EOM_END;
            m_PatternMatchRead = FLUSH;
            return asynSuccess;
    }

    return asynError;
}


asynStatus drvAsynFile::readFilename(asynUser *pasynUser, char *value, size_t maxChars, size_t *nActual, int *eomReason)
{
    switch (m_FilenameRead)
    {
        case FLUSH:
            *nActual = 0;
            *eomReason = ASYN_EOM_END;
            return asynSuccess;

        case NORMAL:
            *nActual = strlen(m_FilenameReadPointer);

            if (*nActual > maxChars)
            {
                *nActual = maxChars;
                *eomReason = ASYN_EOM_CNT;
            }
            else
            {
                *eomReason = ASYN_EOM_END;
                m_FilenameRead = EOS;
            }

            strncpy(value, m_FilenameReadPointer, *nActual);
            m_FilenameReadPointer += *nActual;

            return asynSuccess;

        case EOS:
            *nActual   = 0;
            *eomReason = ASYN_EOM_END;
            return asynSuccess;
    }

    return asynError;
}


asynStatus drvAsynFile::readFile(asynUser *pasynUser, char *value, size_t maxChars, size_t *nActual, int *eomReason)
{
    *nActual = 0;

    if (m_NumBytesRead >= m_MaxFilesize)
    {
        *eomReason = ASYN_EOM_END;
        return asynSuccess;
    }

    if (pasynUser->drvUser == NULL)
    {
        pasynManager->exceptionDisconnect(pasynUser);

        return asynDisconnected;
    }

    FILE *f = (FILE*)pasynUser->drvUser;

    *nActual = fread(value, 1, (m_NumBytesRead + maxChars <= m_MaxFilesize) ? maxChars : m_MaxFilesize - m_NumBytesRead, f);
    m_NumBytesRead += *nActual;

    if (*nActual == maxChars)
        *eomReason = ASYN_EOM_CNT;
    else if (*nActual == 0)
    {
        if (feof(f))
            *eomReason = ASYN_EOM_END;
        else if (ferror(f))
        {
            int lerrno = errno;
            ASYN_ERROR("Read error: %s", strerror(lerrno));

            return asynError;
        }
    }
    else
        *eomReason = ASYN_EOM_EOS;

    return asynSuccess;
}


asynStatus drvAsynFile::writePattern(asynUser *pasynUser, const char *value, size_t maxChars, size_t *nActual)
{
    int patternReady = 0;

    *nActual = maxChars;

    if (maxChars == 0)
        return asynSuccess;

    if (value[0] == STX)
    {
        m_Pattern.clear();

        ++value;
        --maxChars;

        m_PatternMatchRead = FLUSH;
        m_Pattern = m_ExposedPath;

        delete m_Glob;
        m_Glob = NULL;
    }

    if (value[maxChars - 1] == ETX)
    {
        patternReady = 1;
        --maxChars;
    }

    m_Pattern.append(value, maxChars);

    if (patternReady)
    {
        asynPrint(pasynUser, ASYN_TRACE_FLOW, "%s:%s:, pasynUser=%p, registering pattern '%s'\n", driverName, __FUNCTION__, pasynUser, m_Pattern.c_str());

        if (m_IOIntrPatternMatch)
            callCallbacks();
        else
            m_PatternMatchRead = NORMAL;
    }

    return asynSuccess;
}


asynStatus drvAsynFile::writeFilename(asynUser *pasynUser, const char *value, size_t maxChars, size_t *nActual)
{
    *nActual = maxChars;

    m_Filename.append(value, maxChars);

    asynPrint(pasynUser, ASYN_TRACE_FLOW, "%s:%s:, pasynUser=%p, registering filename '%s'\n", driverName, __FUNCTION__, pasynUser, m_Filename.c_str());

    m_FilenameRead = NORMAL;
    m_FilenameReadPointer = m_Filename.c_str();

    return asynSuccess;
}


void drvAsynFile::createGlob()
{
    if (m_Glob == NULL)
    {
        m_PatternMatchPointer = NULL;
        m_Glob = new Glob(m_Pattern.c_str());
        m_Pattern.clear();
    }
}


asynStatus drvAsynFile::callCallbacks()
{
    asynStandardInterfaces *pInterfaces = getAsynStdInterfaces();

    /* Pass octet interrupts */
    if (!pInterfaces->octetInterruptPvt)
    {
        return asynError;
    }

    createGlob();

    epicsTimeStamp timeStamp;
    getTimeStamp(&timeStamp);

    ELLLIST *pclientList;
    pasynManager->interruptStart(pInterfaces->octetInterruptPvt, &pclientList);

    const char etx[] = {ETX, 0};
    for (interruptNode *pnode = (interruptNode *)ellFirst(pclientList);
         pnode;
         pnode = (interruptNode *)ellNext(&pnode->node))
    {
        asynOctetInterrupt *pInterrupt = (asynOctetInterrupt *) pnode->drvPvt;
        int address;

        pasynManager->getAddr(pInterrupt->pasynUser, &address);
        if (address == -1)
            address = 0;

        if (address != List)
            continue;

        /* Set the status for the callback */
        pInterrupt->pasynUser->auxStatus = asynSuccess;
        pInterrupt->pasynUser->alarmStatus = 0;
        pInterrupt->pasynUser->alarmSeverity = 0;
        /* Set the timestamp for the callback */
        pInterrupt->pasynUser->timestamp = timeStamp;

        const char *value = "";

        m_Glob->reset();

        while (value[0] != ETX && (value = m_Glob->getNextFilename()))
        {
            int len;
            int eom = ASYN_EOM_EOS;

            if (value[0] == '\0')
            {
                value = etx;
                eom   = ASYN_EOM_END;
                len   = 1;
            }
            else
                len = strlen(value) + 1;

            pInterrupt->callback(pInterrupt->userPvt,
                                 pInterrupt->pasynUser,
                                 (char*)value, len, eom);
        }
    }

    pasynManager->interruptEnd(pInterfaces->octetInterruptPvt);

    delete m_Glob;
    m_Glob = NULL;

    return asynSuccess;
}


asynStatus drvAsynFile::openFile(asynUser *pasynUser)
{
    if (m_Filename.empty())
    {
        ASYN_ERROR("No file specified");

        return asynError;
    }

    if (pasynUser->drvUser)
    {
        ASYN_ERROR("Disconnect first\n");

        return asynError;
    }

    std::string filename;
    filename = m_ExposedPath + m_Filename;

    FILE *f = fopen(filename.c_str(), "rb");

    pasynUser->drvUser = f;

    if (f == NULL)
    {
        int lerrno = errno;
        asynPrint(pasynUser, ASYN_TRACE_ERROR, "%s:%s:, pasynUser=%p Cannot open '%s': %s\n", driverName, __FUNCTION__, pasynUser, filename.c_str(), strerror(lerrno));
        ASYN_ERROR("Cannot open '%s': %s", filename.c_str(), strerror(lerrno));

        return asynError;
    }

    m_NumBytesRead = 0;

    return asynSuccess;
}


asynStatus drvAsynFile::connect(asynUser *pasynUser)
{
    int addr;
    asynStatus status;

    status = getAddress(pasynUser, &addr);
    if (status != asynSuccess)
       return status;

    switch (addr)
    {
        case Filename:
                pasynManager->exceptionConnect(pasynUser);
                m_FilenameRead = FLUSH;
                m_Filename.clear();

                return asynSuccess;

        case Data:
                if (m_InSession)
                {
                    ASYN_ERROR("Disconnect first\n");

                    return asynError;
                }

                if ((status = openFile(pasynUser)) != asynSuccess)
                    return status;

                m_InSession = true;
                pasynManager->exceptionConnect(pasynUser);
                asynPrint(pasynUser, ASYN_TRACE_FLOW, "%s:%s:, pasynUser=%p\n", driverName, __FUNCTION__, pasynUser);

                return asynSuccess;
    }

    ASYN_ERROR("Cannot connect to address %d!", addr);

    return asynError;
}


asynStatus drvAsynFile::disconnect(asynUser *pasynUser)
{
    int addr;
    asynStatus status;

    status = getAddress(pasynUser, &addr);
    if (status != asynSuccess)
        return status;

    switch (addr)
    {
            case Filename:
                    pasynManager->exceptionDisconnect(pasynUser);
                    m_FilenameRead = FLUSH;

                    return asynSuccess;

            case Data:
                    m_InSession = false;
                    if (pasynUser->drvUser)
                    {
                            fclose((FILE*)pasynUser->drvUser);
                            pasynUser->drvUser = NULL;
                    }

                    pasynManager->exceptionDisconnect(pasynUser);
                    asynPrint(pasynUser, ASYN_TRACE_FLOW, "%s:%s:, pasynUser=%p\n", driverName, __FUNCTION__, pasynUser);

                    return asynSuccess;
    }

    ASYN_ERROR("Cannot disconnect from address %d!", addr);

    return asynError;
}


drvAsynFile::Glob::Glob(const char *pattern) :
    m_Glob(),
    m_Index(0)
{
    int ret;

    if ((ret = glob(pattern, GLOB_MARK, NULL, &m_Glob)) && ret != GLOB_NOMATCH)
    {
        int lerrno = errno;

        m_Glob.gl_pathv = NULL;
        errlogPrintf("Failed to match pattern: %s", strerror(lerrno));
    } 
}


drvAsynFile::Glob::~Glob()
{
    if (m_Glob.gl_pathv)
        globfree(&m_Glob);
}


const char* drvAsynFile::Glob::getNextFilename()
{
    const char *nofile = "";

    if (m_Glob.gl_pathv == NULL)
    {
        return nofile;
    }

    const char *path = m_Glob.gl_pathv[m_Index++];

    if (path == NULL)
    {
        --m_Index;
        return nofile;
    }

    const char *filename = strrchr(path, '/');
    if (filename == NULL)
        return path;

    return filename + 1;
}


void drvAsynFile::Glob::reset()
{
    m_Index = 0;
}


extern "C"
{

static void drvAsynFileConfigure(const char *portname, const char *path_to_expose, int max_filesize, int no_iointr_pattern_match)
{
    if (path_to_expose == NULL || path_to_expose[0] == '\0')
    {
        errlogPrintf("Invalid path\n");
        return;
    }

    new drvAsynFile(portname, path_to_expose, max_filesize, no_iointr_pattern_match);
}


static void drvAsynFileMaxFilesize(const char *portname, int max_filesize)
{
    drvAsynFile *port = reinterpret_cast<drvAsynFile*>(findAsynPortDriver(portname));

    if (port == NULL)
    {
        errlogPrintf("No such port: %s\n", portname);
        return;
    }

    port->setMaxFilesize(max_filesize);
}


/*
 * IOC shell command registration
 */
static const iocshArg drvAsynFileArg0             = { "port name",               iocshArgString};
static const iocshArg drvAsynFileConfigureArg1    = { "path_to_expose",          iocshArgString};
static const iocshArg drvAsynFileMaxFilesizeArg1  = { "max_filesize",            iocshArgInt};
static const iocshArg drvAsynFileConfigureArg3    = { "no_iointr_pattern_match", iocshArgInt};
static const iocshArg *drvAsynFileConfigureArgs[] = {
    &drvAsynFileArg0,
    &drvAsynFileConfigureArg1,
    &drvAsynFileMaxFilesizeArg1,
    &drvAsynFileConfigureArg3
};
static const iocshFuncDef drvAsynFileConfigureFuncDef = {
    "drvAsynFileConfigure",
    sizeof(drvAsynFileConfigureArgs) / sizeof(drvAsynFileConfigureArgs[0]),
    drvAsynFileConfigureArgs
};


static void drvAsynFileConfigureCallFunc(const iocshArgBuf *args)
{
    drvAsynFileConfigure(args[0].sval, args[1].sval, args[2].ival, args[3].ival);
}


static const iocshArg *drvAsynFileMaxFilesizeArgs[] = {
    &drvAsynFileArg0,
    &drvAsynFileMaxFilesizeArg1,
};
static const iocshFuncDef drvAsynFileMaxFilesizeFuncDef = {
    "drvAsynFileMaxFilesize",
    sizeof(drvAsynFileMaxFilesizeArgs) / sizeof(drvAsynFileMaxFilesizeArgs[0]),
    drvAsynFileMaxFilesizeArgs
};


static void drvAsynFileMaxFilesizeCallFunc(const iocshArgBuf *args)
{
    drvAsynFileMaxFilesize(args[0].sval, args[1].ival);
}

/*
 * This routine is called before multitasking has started, so there's
 * no race condition in the test/set of firstTime.
 */
static void drvAsynFileRegister(void)
{
    static int firstTime = 1;

    if (firstTime)
    {
        iocshRegister(&drvAsynFileConfigureFuncDef, drvAsynFileConfigureCallFunc);
        iocshRegister(&drvAsynFileMaxFilesizeFuncDef, drvAsynFileMaxFilesizeCallFunc);

        firstTime = 0;
    }
}
epicsExportRegistrar(drvAsynFileRegister);

}
